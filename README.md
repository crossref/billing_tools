# Billing Tools

Billing tools. Currently for Similarity Check invoice processing.

## Test

To run test:

    mvn test

## Docker Build

To build and run locally on port 8888:

    docker build -t billingtools .
    docker run  --env "SERVER_PORT=8888" --publish 8888 billingtools
 
## Run with Docker Compose

The Docker Compose file has 

    docker-compose run --service-ports run
 
## Setup

Docker build is self-contained and fetches its own dependency. If you want to run the build directly on Mac OS you may need to install the latest Java and Maven:

    brew install openjdk
    brew cask install adoptopenjdk
    brew install maven

Using IntelliJ IDEA, basing a file off the Maven `pom.xml`, everything should be automatic.
