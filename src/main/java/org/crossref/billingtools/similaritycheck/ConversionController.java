package org.crossref.billingtools.similaritycheck;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.util.Collection;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ConversionController {

  @GetMapping("/upload")
  public String upload() {
    return "upload";
  }

  @PostMapping("/upload")
  public ResponseEntity singleFileUpload(@RequestParam("file") MultipartFile file,
      RedirectAttributes redirectAttributes) {

    try {
      // Get the file and save it somewhere
      Collection<Invoice> billingData = Processor.load(file.getInputStream());

      String responseFilename = file.getOriginalFilename().replace(" ", "_")
          .replaceFirst("\\.csv$", "_out.csv");

      BillingContext billingContext = new BillingContext(Clock.systemUTC());
      byte[] byteArray = Processor.convertFile(billingData, billingContext);
      return ResponseEntity.ok()
          .contentType(new MediaType("text", "csv", StandardCharsets.UTF_8))
          .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + responseFilename)
          .body(byteArray);

    } catch (IOException | IllegalArgumentException e) {
        e.printStackTrace();
      return ResponseEntity.badRequest()
          .contentType(new MediaType("text", "plain", StandardCharsets.UTF_8))
          .body(String.format("Error: Failed to read CSV: %s", e.getMessage()));
    }
  }

  @GetMapping("/error")
  public String error() {
    return "error";
  }
}
