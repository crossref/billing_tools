package org.crossref.billingtools.similaritycheck;

import java.time.Clock;
import java.time.LocalDate;

public class BillingContext {

  private LocalDate exchangeRateDate;
  private LocalDate creationDate;

  public BillingContext(Clock clock) {
    LocalDate now = LocalDate.now(clock);

    // Exchange rate is day of the run.
    exchangeRateDate = now;

    // Last day of previous year
    creationDate = LocalDate.of(now.getYear(), 1, 1).minusDays(1);

  }

  public LocalDate getExchRateDate() {
    return exchangeRateDate;
  }

  public LocalDate getCreationDate() {
    return creationDate;
  }

}
