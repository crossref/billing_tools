package org.crossref.billingtools.similaritycheck;

import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;

public class Invoice {

  private final String intacctId;
  private final List<InputRow> rows;

  public Invoice(String intacctId, List<InputRow> rows) {
    this.intacctId = intacctId;
    this.rows = rows;
  }

  public List<InputRow> getInvoiceRows() {
    return this.rows;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null) {
      return false;
    }

    if (getClass() != o.getClass()) {
      return false;
    }
    Invoice other = (Invoice) o;

    return this.intacctId.equals(other.intacctId) && CollectionUtils
        .isEqualCollection(this.rows, other.rows);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.intacctId, this.rows);
  }

  @Override
  public String toString() {
    return String.format("Invoice. Intacct: = %s, rows: %s", this.intacctId, this.rows.toString());
  }
}

