package org.crossref.billingtools.similaritycheck;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.csv.CSVPrinter;

public class OutputRow {

  // Constants for fixed fields in invoice item lines
  public static final String TRANSACTIONTYPE = "Sales Invoice"; //1
  public static final String CREATEDFROM = ""; //3
  public static final String DOCUMENTNO = ""; //5
  public static final String REFERENCENO = ""; //6
  public static final String TERMNAME = "N45"; //7
  public static final String DATEDUE = ""; //8
  public static final String SHIPPINGMETHOD = ""; //10
  public static final String SHIPTO_CONTACTNAME = ""; //11
  public static final String BILLTO_CONTACTNAME = ""; //12
  public static final String SHIPTO = ""; //13
  public static final String BILLTO = ""; //14
  public static final String CURRENCY = "USD"; //15
  public static final String BASECURRENCY = "USD"; //16
  public static final String EXCHRATETYPE = "Intacct Daily Rate"; //18
  public static final String EXCHRATE = ""; //19
  public static final String VSOEPRICELIST = ""; //20
  public static final String BUNDLENUMBER = ""; //21
  public static final String ITEMID = "40825"; // 22
  public static final String WAREHOUSEID = "";// 24
  public static final String UNIT = "EA";// 26
  public static final String DISCSURCHARGEMEMO = "";// 28
  public static final String LOCATIONID = "CrossRef - US";// 29
  public static final String DEPARTMENTID = "";// 30
  public static final String REVRECTEMPLATE = "";// 32
  public static final String REVRECSTARTDATE = "";// 33
  public static final String REVRECENDDATE = "";// 34
  public static final String RENEWALMACRO = "c12 - Jan - Dec";// 35
  public static final String L_PROJECTID = "";// 36
  public static final String L_CUSTOMERID = "";// 37
  public static final String L_VENDORID = "";// 38
  public static final String L_EMPLOYEEID = "";// 39
  public static final String L_CLASSID = "";// 40
  public static final String ST_DESCRIPTION = "";// 41
  public static final String ST_TOTAL = "";// 42
  public static final String ST_PERCENTVAL = "";// 43
  public static final String ST_LOCATIONID = "";// 44
  // Header column labels
  private static final List<String> HEADERS = Arrays
      .asList("transactiontype", "datecreated", "createdfrom", "customerid", "documentno",
          "referenceno",
          "termname", "datedue", "message", "shippingmethod", "shipto_contactname",
          "billto_contactname", "shipto", "billto", "currency", "basecurrency",
          "exchratedate", "exchratetype", "exchrate", "vsoepricelist", "bundlenumber", "itemid",
          "itemdesc", "warehouseid", "quantity", "unit", "price",
          "discsurchargememo", "locationid", "departmentid", "memo", "revrectemplate",
          "revrecstartdate", "revrecenddate", "renewalmacro", "l_projectid",
          "l_customerid", "l_vendorid", "l_employeeid", "l_classid", "st_description", "st_total",
          "st_percentval", "st_locationid");
  private static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
  private final BillingContext billingContext;
  private final String name;
  private final String turnitinId;
  private final String intacctId;
  private final String message;
  private final String itemDesc;
  private final Long numberCheckedBillable;
  private final BigDecimal unitPrice;
  private final String memo;
  public OutputRow(String name, String intacctId, String turnitinId, BigDecimal unitPrice,
      Long numberCheckedBillable, BillingContext billingContext) {
    String badDataPattStr = "(?sm)([\\u00A1-\\u00A7\\u00A9-\\u00AF\\u00B1-\\u00B3\\u00C2\\u00C5\\u00B6\\u00BB\\u009B\\u0082-\\u0084])";
    Pattern badDataPatt = Pattern.compile(badDataPattStr);
    Matcher matcher = badDataPatt.matcher(name);
    if (matcher.find()) {
      this.name = new String(name.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    } else {
      this.name = name;
    }
    this.intacctId = intacctId;
    this.turnitinId = turnitinId;
    this.unitPrice = unitPrice;
    this.numberCheckedBillable = numberCheckedBillable;
    this.message = "Annual Usage: " + this.name + " (Turnitin ID:" + turnitinId + ")";
    this.itemDesc =
        "Billable SimChecks (Turnitin ID:" + turnitinId + ") [price " + unitPrice + " per unit]";
    this.memo = message;
    this.billingContext = billingContext;
  }

  public static void printHeadersTo(CSVPrinter printer) throws IOException {
    printer.printRecord(HEADERS);
  }

  public String getName() {
    return name;
  }

  public String getTurnitinId() {
    return turnitinId;
  }

  public String getIntacctId() {
    return intacctId;
  }

  public String getMessage() {
    return message;
  }

  public String getItemDesc() {
    return itemDesc;
  }

  public Long getNumberCheckedBillable() {
    return numberCheckedBillable;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public String getMemo() {
    return memo;
  }

  public void printTo(CSVPrinter printer) throws IOException {
    printer.printRecord(
        TRANSACTIONTYPE,
        dateFormat.format(billingContext.getCreationDate()),
        CREATEDFROM,
        getIntacctId(),
        DOCUMENTNO,
        REFERENCENO,
        TERMNAME,
        DATEDUE,
        getMessage(),
        SHIPPINGMETHOD,
        SHIPTO_CONTACTNAME,
        BILLTO_CONTACTNAME,
        SHIPTO,
        BILLTO,
        CURRENCY,
        BASECURRENCY,
        dateFormat.format(billingContext.getExchRateDate()),
        EXCHRATETYPE,
        EXCHRATE,
        VSOEPRICELIST,
        BUNDLENUMBER,
        ITEMID,
        getItemDesc(),
        WAREHOUSEID,
        getNumberCheckedBillable().toString(),
        UNIT,
        getUnitPrice().toString(),
        DISCSURCHARGEMEMO,
        LOCATIONID,
        DEPARTMENTID,
        getMemo(),
        REVRECTEMPLATE,
        REVRECSTARTDATE,
        REVRECENDDATE,
        RENEWALMACRO,
        L_PROJECTID,
        L_CUSTOMERID,
        L_VENDORID,
        L_EMPLOYEEID,
        L_CLASSID,
        ST_DESCRIPTION,
        ST_TOTAL,
        ST_PERCENTVAL,
        ST_LOCATIONID);
  }

  public String toString() {
    return String.format("Intacct: %s, Turnitin: %s, Name: %s, UnitPrice: %f, Num Checked: %d",
        this.intacctId,
        this.turnitinId,
        this.name,
        this.unitPrice,
        this.numberCheckedBillable
    );
  }
}

