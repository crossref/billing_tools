package org.crossref.billingtools.similaritycheck;

import java.math.BigDecimal;
import java.util.Objects;
import org.apache.commons.csv.CSVRecord;

public class InputRow {

  public static final String ACCOUNT_NAME = "Account Name";
  public static final String INTACCT_ID = "Crossref ID number";
  public static final String TURNITIN_ID = "Account ID";
  public static final String UNIT_PRICE = "Unit Price";
  public static final String NUMBER_CHECKED_BILLABLE = "Less 100 credit";
  public static final String TOTAL_FEE = "Price";
  public static final String PROBLEM_INTACCT_IDS = "SPST000|LWW00";
  private final String name;
  private final String intacctId;
  private final String turnitinId;
  private final BigDecimal unitPrice;
  private final Long numberCheckedBillable;
  private final BigDecimal totalFee;
  public InputRow(String name, String intacctId, String turnitinId, BigDecimal unitPrice,
      Long numberCheckedBillable, BigDecimal totalFee) {
    this.name = name;
    this.intacctId = assertValid(intacctId);
    this.turnitinId = turnitinId;
    this.unitPrice = unitPrice;
    this.numberCheckedBillable = numberCheckedBillable;
    this.totalFee = totalFee;
  }

  public static InputRow fromRow(CSVRecord record) throws NumberFormatException {
    BigDecimal unitPrice;
    long billableCount = 0;
    BigDecimal totalFee;
    try {
      totalFee = new BigDecimal(record.get(TOTAL_FEE).replaceAll("[$, ]", ""));
    } catch (Exception e) {
      throw new NumberFormatException("Can not format monetary value: " + record.get(TOTAL_FEE));
    }

    try {
      unitPrice = new BigDecimal(record.get(UNIT_PRICE).replaceAll("[$, ]", ""));
    } catch (Exception e) {
      if (totalFee.compareTo(new BigDecimal(0)) != 0) {
        throw new NumberFormatException("Can not format monetary value: " + record.get(UNIT_PRICE));
      } else {
        unitPrice = new BigDecimal(0);
      }
    }
    try {
      billableCount = Long.parseLong(record.get(NUMBER_CHECKED_BILLABLE).replaceAll("[, ]", ""));
    } catch (Exception e) {
      throw new NumberFormatException(
          "Can not format item count: " + record.get(NUMBER_CHECKED_BILLABLE));
    }
    return new InputRow(record.get(ACCOUNT_NAME),
        record.get(INTACCT_ID),
        record.get(TURNITIN_ID),
        unitPrice,
        billableCount,
        totalFee);
  }

  public String getName() {
    return name;
  }

  public String getIntacctId() {
    return intacctId;
  }

  public String getTurnitinId() {
    return turnitinId;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  Long getNumberCheckedBillable() {
    return numberCheckedBillable;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public String toString() {
    return String
        .format("Intacct: %s, Name: %s, TurnitinId: %s, UnitPrice: %f, Num Checked: %d, Total: %f",
            this.intacctId,
            this.name,
            this.turnitinId,
            this.unitPrice,
            this.numberCheckedBillable,
            this.totalFee);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(this.name, this.intacctId, this.turnitinId, this.numberCheckedBillable, this.totalFee,
            this.unitPrice);
  }

  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }

    if (o.getClass() != this.getClass()) {
      return false;
    }

    InputRow other = (InputRow) o;
    return this.intacctId.equals(other.intacctId) &&
        this.numberCheckedBillable.equals(other.numberCheckedBillable) &&
        this.turnitinId.equals(other.turnitinId) &&
        this.totalFee.equals(other.totalFee) &&
        this.name.equals(other.name) &&
        this.unitPrice.equals(other.unitPrice);
  }

  private String assertValid(String intacctId) {
    // special case for 2 long standing errors
    if (!intacctId.matches("[A-Z]{3,4}[0]{1,2}[0-1]") && !intacctId.matches(PROBLEM_INTACCT_IDS)) {
      throw new IllegalArgumentException("Invalid Intacct ID format (not AAAA00): " + intacctId);
    }
    return intacctId;
  }
}
