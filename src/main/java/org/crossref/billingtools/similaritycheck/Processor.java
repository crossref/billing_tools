package org.crossref.billingtools.similaritycheck;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;

public class Processor {

  private Processor() {

  }

  public static Collection<Invoice> load(InputStream input) throws IOException {
    try (CSVParser parser = CSVParser
        .parse(new BOMInputStream(input, ByteOrderMark.UTF_8), StandardCharsets.UTF_8,
            CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
      List<CSVRecord> records = parser.getRecords();
      List<InputRow> rowList = new ArrayList<>();
      Map<String, List<InputRow>> intacctGroups;
      for (CSVRecord csvrecord : records) {
        if (csvrecord.get(InputRow.INTACCT_ID).length() > 0
            && csvrecord.get(InputRow.TURNITIN_ID).length() > 0) { // suppress blank lines
          try {
            InputRow row = InputRow.fromRow(csvrecord);
            if (row.getNumberCheckedBillable() > 0L) { // suppress lines with nothing billable
              rowList.add(row);
            }
          } catch (Exception e) {
            throw new IOException("Bad record: " + csvrecord.toString() + "<br/>" + e);
          }
        }
      }
      intacctGroups = rowList.stream().collect(Collectors.groupingBy(InputRow::getIntacctId));

      // Group into one invoice per Intacct ID.
      return intacctGroups
          .entrySet()
          .stream()
          .map(e -> new Invoice(e.getKey(), e.getValue()))
          .collect(Collectors.toList());
    }
  }

  public static byte[] convertFile(Collection<Invoice> billingData, BillingContext billingContext)
      throws IOException {
    CharArrayWriter stream = new CharArrayWriter();

    CSVPrinter csvPrinter = new CSVPrinter(stream, CSVFormat.RFC4180);
    OutputRow.printHeadersTo(csvPrinter);

    for (Invoice invoice : billingData) {
      List<InputRow> rows = invoice.getInvoiceRows();
      for (InputRow inputRow : rows) {
        OutputRow outputRow = new OutputRow(
            inputRow.getName(),
            inputRow.getIntacctId(),
            inputRow.getTurnitinId(),
            inputRow.getUnitPrice(),
            inputRow.getNumberCheckedBillable(),
            billingContext);
        outputRow.printTo(csvPrinter);
      }
    }
    return stream.toString().getBytes(StandardCharsets.UTF_8);
  }
}
