package org.crossref.billingtools;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.math.BigDecimal;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.crossref.billingtools.similaritycheck.InputRow;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
class InputRowTests {

  private CSVRecord parseRow(String input) throws IOException {
    return CSVParser.parse(input, CSVFormat.RFC4180.withFirstRecordAsHeader()).getRecords().get(0);
  }

  @Test
  @DisplayName("Row with dollar in price should parse ok.")
  void validCurrencyFormat() throws IOException {
    String input =
        "Account ID,Crossref ID number,Account Name,DOI Prefix,Less 100 credit,Unit Price,Price\n"
            + "1,ABCR00,American Bee Center,10.1111,100,$0.75,$75.00\n";

    CSVRecord record = parseRow(input);

    assertThat(InputRow.fromRow(record).getUnitPrice()).isEqualTo(new BigDecimal("0.75"));
    assertThat(InputRow.fromRow(record).getTotalFee()).isEqualTo(new BigDecimal("75.00"));
  }

  @Test
  @DisplayName("Row with invalid price should throw error.")
  void invalidCurrencyFormat() throws IOException {
    String input =
        "Account ID,Crossref ID number,Account Name,DOI Prefix,Less 100 credit,Unit Price,Price\n"
            + "1,ABCR00,American Bee Center,10.1111,100,0.75,NOT_A_TOTAL\n";

    assertThrows(NumberFormatException.class, () -> {
      assertThat(InputRow.fromRow(parseRow(input)));
    });
  }

  @Test
  @DisplayName("Row with invalid count should throw error.")
  void invalidCountFormat() throws IOException {
    String input =
        "Account ID,Crossref ID number,Account Name,DOI Prefix,Less 100 credit,Unit Price,Price\n"
            + "1,ABCR00,American Bee Center,10.1111,NOT_A_COUNT,0.75,70.0\n";

    assertThrows(NumberFormatException.class, () -> {
      assertThat(InputRow.fromRow(parseRow(input)));
    });
  }

  @Test
  @DisplayName("Known 'good' legacy Intacct IDs which don't follow rule should not raise error.")
  void knownInvalidIntacctId() throws IOException {
    String input =
        "Account ID,Crossref ID number,Account Name,DOI Prefix,Less 100 credit,Unit Price,Price\n"
            + "1,SPST000,American Bee Center,10.1111,123,0.75,70.0\n";

    assertThat(InputRow.fromRow(parseRow(input)).getIntacctId()).isEqualTo("SPST000");
  }

  @DisplayName("Row with known bad Intacct Format should throw error.")
  @Test
  void invalidIntacctIdFormat() throws IOException {
    String input =
        "Account ID,Crossref ID number,Account Name,DOI Prefix,Less 100 credit,Unit Price,Price\n"
            + "1,NOT_AN_INTACCT_ID,American Bee Center,10.1111,123,0.75,70.0\n";

    assertThrows(IllegalArgumentException.class, () -> {
      InputRow.fromRow(parseRow(input));
    });
  }

  @DisplayName("If the unit price is missing but the total is zero, return unit price of zero.")
  @Test
  void missingUnitPriceZeroIfTotalZero() throws IOException {
    String input =
        "Account ID,Crossref ID number,Account Name,DOI Prefix,Less 100 credit,Unit Price,Price\n"
            + "1,ABCR00,American Bee Center,10.1111,123,,0.0\n";

    CSVRecord record = parseRow(input);
    assertThat(InputRow.fromRow(record).getTotalFee()).isEqualTo(new BigDecimal("0.0"));
    assertThat(InputRow.fromRow(record).getUnitPrice()).isEqualTo(new BigDecimal("0"));
  }
}
