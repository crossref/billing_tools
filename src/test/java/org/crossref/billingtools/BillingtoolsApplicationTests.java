package org.crossref.billingtools;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.crossref.billingtools.similaritycheck.BillingContext;
import org.crossref.billingtools.similaritycheck.InputRow;
import org.crossref.billingtools.similaritycheck.Invoice;
import org.crossref.billingtools.similaritycheck.OutputRow;
import org.crossref.billingtools.similaritycheck.Processor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BillingtoolsApplicationTests {

  @Test
  void invoiceEquals() {
    Invoice a = new Invoice("ABCR00", Collections.singletonList(
        new InputRow("American Bee Center", "ABCR00", "1", new BigDecimal("0.75"), 100L,
            new BigDecimal("75.00"))));

    Invoice b = new Invoice("ABCR00", Collections.singletonList(
        new InputRow("American Bee Center", "ABCR00", "1", new BigDecimal("0.75"), 100L,
            new BigDecimal("75.00"))));

    // Explicitly call the equals because we're testing that method.
    assertThat(a.hashCode() == b.hashCode()).isTrue();
    assertThat(a.equals(b)).isTrue();
    assertThat(b.equals(a)).isTrue();
    assertThat(a.equals(null)).isFalse();
    assertThat(b.equals(null)).isFalse();
  }

  @Test
  void invoiceNotEquals() {
    Invoice a = new Invoice("ABCR00", Collections.singletonList(
        new InputRow("American Bee Center", "ABCR00", "1", new BigDecimal("0.75"), 100L,
            new BigDecimal("75.00"))));

    // Different Intacct Id.
    Invoice b = new Invoice("XXXX", Collections.singletonList(
        new InputRow("American Bee Center", "ABCR00", "1", new BigDecimal("0.75"), 100L,
            new BigDecimal("75.00"))));

    // Explicitly call the equals because we're testing that method.
    assertThat(a.hashCode() != b.hashCode()).isTrue();
    assertThat(!a.equals(b)).isTrue();
    assertThat(!b.equals(a)).isTrue();
  }

  @Test
    // Simple input produces on invoice per member account when they are all distinct.
  void simple() throws IOException {
    InputStream inputStream = getClass()
        .getClassLoader().getResourceAsStream("similaritycheck/simple.csv");

    Collection<Invoice> expected = Arrays.asList(
        new Invoice("ABCR00", Collections.singletonList(
            new InputRow("American Bee Center", "ABCR00", "1", new BigDecimal("0.75"), 100L,
                new BigDecimal("75.00")))),
        new Invoice("ABCR01", Collections.singletonList(
            new InputRow("Armenian Beetle Centre", "ABCR01", "2", new BigDecimal("0.75"), 200L,
                new BigDecimal("150.00")))),
        new Invoice("ABCN00", Collections.singletonList(
            new InputRow("Associated Bat Corporation", "ABCN00", "3", new BigDecimal("0.75"), 300L,
                new BigDecimal("225.00")))),
        new Invoice("ABCT00", Collections.singletonList(
            new InputRow("American Bee Center", "ABCT00", "4", new BigDecimal("0.75"), 400L,
                new BigDecimal("300.00"))))
    );

    // All invoices should be transformed correctly, regardless of order.
    assertThat(Processor.load(
        inputStream
    )).containsExactlyInAnyOrderElementsOf(expected);
  }

  @Test
    // Input should ignore blank lines and line ordering.
  void blankLines() throws IOException {
    InputStream original = getClass()
        .getClassLoader().getResourceAsStream("similaritycheck/simple.csv");

    InputStream reorderedBlank = getClass()
        .getClassLoader().getResourceAsStream("similaritycheck/simple-blanklines.csv");

    // Both files should parse as equivalent.
    assertThat(Processor.load(original))
        .containsExactlyInAnyOrderElementsOf(Processor.load(reorderedBlank));
  }

  @Test
    // Invoices should be grouped by Intacct ID.
  void groupByIntacct() throws IOException {
    InputStream input = getClass()
        .getClassLoader().getResourceAsStream("similaritycheck/groups.csv");

    Collection<Invoice> expected = Arrays.asList(
        new Invoice("GRUP00", Arrays.asList(
            new InputRow("Sponsored 1", "GRUP00", "1", new BigDecimal("0.75"), 100L,
                new BigDecimal("75.00")),
            new InputRow("Sponsored 2", "GRUP00", "2", new BigDecimal("0.75"), 200L,
                new BigDecimal("150.00")),
            new InputRow("Sponsored 3", "GRUP00", "3", new BigDecimal("0.75"), 300L,
                new BigDecimal("225.00"))
        )),
        new Invoice("ABCD00", Collections.singletonList(
            new InputRow("Individual 1", "ABCD00", "4", new BigDecimal("0.75"), 400L,
                new BigDecimal("300.00")))),
        new Invoice("ABCD01", Collections.singletonList(
            new InputRow("Individual 2", "ABCD01", "5", new BigDecimal("0.75"), 400L,
                new BigDecimal("300.00"))))
    );

    // The correctly grouped invoices should be loaded.
    Collection<Invoice> found = Processor.load(input);
    assertThat(found).containsExactlyInAnyOrderElementsOf(expected);
  }

  @Test
  @DisplayName("BillingContext should create billing dates based on today's date.")
  void billingContext() {
    Clock clock = Clock
        .fixed(LocalDate.of(2020, 6, 15).atStartOfDay().toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
    BillingContext context = new BillingContext(clock);

    // Creation Date should be last day of previous year.
    assertThat(context.getCreationDate()).isEqualTo(LocalDate.of(2019, 12, 31));

    // Exchange Rate Date should be date of the run.
    assertThat(context.getExchRateDate()).isEqualTo(LocalDate.of(2020, 6, 15));

  }

  @Test
    // CSV Row for given puts should parse to expected values.
  void validRow() throws IOException {
    String name = "Adam Mickiewicz University";
    String intacctId = "ADAM00";
    String turnitinId = "71874";
    BigDecimal unitPrice = new BigDecimal("0.75");
    Long recCount = 400L;

    Clock clock = Clock
        .fixed(LocalDate.of(2020, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
    BillingContext context = new BillingContext(clock);

    OutputRow outputRow = new OutputRow(name, intacctId, turnitinId, unitPrice, recCount, context);
    CharArrayWriter stream = new CharArrayWriter();
    CSVPrinter csvPrinter = new CSVPrinter(stream, CSVFormat.RFC4180);
    OutputRow.printHeadersTo(csvPrinter);
    outputRow.printTo(csvPrinter);
    String actualCSV = stream.toString();

    Map<String, String> expected = new HashMap<>();
    expected.put("transactiontype", OutputRow.TRANSACTIONTYPE);
    expected.put("datecreated", "12/31/2019");
    expected.put("createdfrom", OutputRow.CREATEDFROM);
    expected.put("customerid", intacctId);
    expected.put("documentno", OutputRow.DOCUMENTNO);
    expected.put("referenceno", OutputRow.REFERENCENO);
    expected.put("termname", OutputRow.TERMNAME);
    expected.put("datedue", OutputRow.DATEDUE);
    expected.put("message", "Annual Usage: " + name + " (Turnitin ID:" + turnitinId + ")");
    expected.put("shippingmethod", OutputRow.SHIPPINGMETHOD);
    expected.put("shipto_contactname", OutputRow.SHIPTO_CONTACTNAME);
    expected.put("billto_contactname", OutputRow.BILLTO_CONTACTNAME);
    expected.put("shipto", OutputRow.SHIPTO);
    expected.put("billto", OutputRow.BILLTO);
    expected.put("currency", OutputRow.CURRENCY);
    expected.put("basecurrency", OutputRow.BASECURRENCY);
    expected.put("exchratedate", "01/01/2020");
    expected.put("exchratetype", OutputRow.EXCHRATETYPE);
    expected.put("exchrate", OutputRow.EXCHRATE);
    expected.put("vsoepricelist", OutputRow.VSOEPRICELIST);
    expected.put("bundlenumber", OutputRow.BUNDLENUMBER);
    expected.put("itemid", "40825");
    expected.put("itemdesc",
        "Billable SimChecks (Turnitin ID:" + turnitinId + ") [price " + unitPrice + " per unit]");
    expected.put("warehouseid", OutputRow.WAREHOUSEID);
    expected.put("quantity", recCount.toString());
    expected.put("unit", OutputRow.UNIT);
    expected.put("price", "0.75");
    expected.put("discsurchargememo", OutputRow.DISCSURCHARGEMEMO);
    expected.put("locationid", OutputRow.LOCATIONID);
    expected.put("departmentid", OutputRow.DEPARTMENTID);
    expected.put("memo", "Annual Usage: " + name + " (Turnitin ID:" + turnitinId + ")");
    expected.put("revrectemplate", OutputRow.REVRECTEMPLATE);
    expected.put("revrecstartdate", OutputRow.REVRECSTARTDATE);
    expected.put("revrecenddate", OutputRow.REVRECENDDATE);
    expected.put("renewalmacro", OutputRow.RENEWALMACRO);
    expected.put("l_projectid", OutputRow.L_PROJECTID);
    expected.put("l_customerid", OutputRow.L_CUSTOMERID);
    expected.put("l_vendorid", OutputRow.L_VENDORID);
    expected.put("l_employeeid", OutputRow.L_EMPLOYEEID);
    expected.put("l_classid", OutputRow.L_CLASSID);
    expected.put("st_description", OutputRow.ST_DESCRIPTION);
    expected.put("st_total", OutputRow.ST_TOTAL);
    expected.put("st_percentval", OutputRow.ST_PERCENTVAL);
    expected.put("st_locationid", OutputRow.ST_LOCATIONID);

    CSVParser parser = CSVParser.parse(actualCSV, CSVFormat.RFC4180.withFirstRecordAsHeader());
    List<CSVRecord> records = parser.getRecords();
    Map<String, String> actual = records.get(0).toMap();

    assertThat(actual).isEqualTo(expected);
  }

  @Test
    // Verify that encoding issues are handled
  void encodingTest() throws IOException {
    InputStream inputStream = getClass()
        .getClassLoader().getResourceAsStream("similaritycheck/encoding.csv");

    Collection<Invoice> expected = Arrays.asList(
        new Invoice("ABEC00", Arrays.asList(
            new InputRow(
                "Instituto Federal de Educação, Ciência e Tecnologia do Amapá - Ifap - ABEC",
                "ABEC00", "77459", new BigDecimal("0.65"), 1L, new BigDecimal("0.65")),
            new InputRow("CPRM - Serviço Geológico do Brasil", "ABEC00", "162989",
                new BigDecimal("0.65"), 1L, new BigDecimal("0.65")),
            new InputRow("FUNDAÇÃO CENTRO TECNOLÓGICO JUIZ DE FORA (FCT) - ABEC", "ABEC00", "87301",
                new BigDecimal("0.65"), 1L, new BigDecimal("0.65")),
            new InputRow("Universidade Federal de São João del Rei", "ABEC00", "88961",
                new BigDecimal("0.65"), 1L, new BigDecimal("0.65")),
            new InputRow("Revista Direito e Paz", "ABEC00", "186441", new BigDecimal("0.65"), 1L,
                new BigDecimal("0.65")),
            new InputRow("Instituto Brasileiro de Relações Internacionais​", "ABEC00", "88959",
                new BigDecimal("0.65"), 1L, new BigDecimal("0.65")))),
        new Invoice("INSR00", Collections.singletonList(new InputRow(
            "Instytut Ekonomiki Rolnictwa i Gospodarki Żywnościowej - Państwowy Instytut Badawczy",
            "INSR00", "167457", new BigDecimal("0.75"), 1L, new BigDecimal("0.75"))))
    );
    Collection<Invoice> found = Processor.load(inputStream);

    Clock clock = Clock
        .fixed(LocalDate.of(2020, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC), ZoneOffset.UTC);
    BillingContext context = new BillingContext(clock);

    byte[] foundByteArray = Processor.convertFile(found, context);
    byte[] expectedByteArray = Processor.convertFile(expected, context);

    assertThat(foundByteArray).containsExactly(expectedByteArray);
  }

  @Test
    // Verify that encoding issues are handled
  void fileBOMTest() throws IOException {
    InputStream inputStream = getClass()
        .getClassLoader().getResourceAsStream("similaritycheck/bom.csv");

    Collection<Invoice> found = Processor.load(inputStream);

    assertThat(found.size()).isEqualTo(2);
    for (Invoice invoice : found) {
      for (InputRow inputRow : invoice.getInvoiceRows()) {
        assertThat(inputRow.getTurnitinId()).isNotEmpty();
      }
    }
  }
}
