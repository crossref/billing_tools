package org.crossref.billingtools;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTests {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void shouldReturnUploadFormOk() throws Exception {
    this.mockMvc.perform(get("/upload")).andExpect(status().isOk());
  }

  @Test
  public void errorPageShouldexist() throws Exception {
    this.mockMvc.perform(get("/error")).andExpect(status().isOk());
  }

  @Test
  public void shouldErrorEmptyFile() throws Exception {
    this.mockMvc.perform(multipart("/upload").file("this", "".getBytes(StandardCharsets.UTF_8)))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void shouldHandleUploadFileAndReturnCSV() throws Exception {
    byte[] csv = "Account ID,Crossref ID number,Account Name,DOI Prefix,Less 100 credit,Unit Price,Price\n1,GRUP00,Sponsored 1,10.1111,100,0.75,$75.00\n"
        .getBytes(StandardCharsets.UTF_8);
    this.mockMvc.perform(multipart("/upload").file("file", csv))
        .andExpect(status().is2xxSuccessful())
        .andExpect(content()
            .contentTypeCompatibleWith(new MediaType("text", "csv", StandardCharsets.UTF_8)));
  }

  @Test
  public void shouldErrorBadCSV() throws Exception {
    byte[] csv = "this,is,bad\ncsv".getBytes(StandardCharsets.UTF_8);
    this.mockMvc.perform(multipart("/upload").file("the filename.csv", csv))
        .andExpect(status().is4xxClientError());
  }
}
