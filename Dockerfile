# FROM openjdk:8-jdk-alpine
FROM maven:3.6.2-jdk-11
COPY . /tmp/source/
WORKDIR /tmp/source/
RUN mvn package
RUN mv /tmp/source/target/*.jar /app.jar
RUN rm -rf /tmp/source
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]